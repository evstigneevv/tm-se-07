package ru.evstigneev.tm.api;

import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;

import java.util.Collection;

public interface IUserService {

    User create(final String login, final String password) throws Exception;

    User create(final String login, final String password, final RoleType role) throws Exception;

    Collection<User> findAll();

    boolean checkPassword(final String login, final String password) throws Exception;

    boolean updatePassword(final String login, final String newPassword) throws Exception;

    boolean isAdmin(final User user) throws Exception;

    User findByLogin(final String login) throws Exception;

    void update(final String userId, final String userName) throws Exception;

    boolean remove(final String userId) throws Exception;

    User persist(final String userId, final User user);

    void removeAll();

    User merge(final String userId, final User user);

}
