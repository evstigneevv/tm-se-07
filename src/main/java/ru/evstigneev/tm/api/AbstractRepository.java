package ru.evstigneev.tm.api;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractRepository<V> {

    protected final Map<String, V> entities = new LinkedHashMap<>();

    public abstract V merge(final String userId, final V value);

    public abstract V persist(final String userId, final V value);

    public abstract void removeAll();

    public abstract Collection<V> findAll();

}
