package ru.evstigneev.tm.api;

import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.Collection;

public interface IProjectService {

    Collection<Project> findAll() throws Exception;

    Project create(final String userId, final String projectName) throws EmptyStringException;

    boolean remove(final String userId, final String projectId) throws EmptyStringException;

    Project update(final String userId, final String projectId, final String newProjectName) throws EmptyStringException;

    Project findOne(final String userId, final String projectId) throws EmptyStringException;

    Project merge(final String userId, final Project project) throws Exception;

    Project persist(final String userId, final Project project) throws Exception;

    Collection<Project> findAllByUserId(final String userId) throws Exception;

    void removeAll();

    boolean removeAllByUserId(final String userId) throws Exception;

}
