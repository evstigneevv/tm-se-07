package ru.evstigneev.tm.api;

import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.Collection;

public interface ITaskService {

    Task create(final String userId, final String taskName, final String projectId) throws EmptyStringException;

    Collection<Task> findAll();

    Collection<Task> findAllByUserId(final String userId) throws Exception;

    boolean remove(final String userId, final String taskId) throws EmptyStringException;

    Collection<Task> getTaskListByProjectId(final String userId, final String projectId) throws EmptyStringException;

    Task update(final String userId, final String taskId, final String taskName) throws EmptyStringException;

    Task findOne(final String userId, final String taskId) throws EmptyStringException;

    Task merge(final String userId, final Task task) throws Exception;

    Task persist(final String userId, final Task task) throws Exception;

    void removeAll();

    boolean removeAllByUserId(final String userId) throws Exception;

    boolean deleteAllProjectTasks(final String userId, final String projectId) throws Exception;

}
