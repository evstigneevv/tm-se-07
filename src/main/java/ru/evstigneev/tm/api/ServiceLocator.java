package ru.evstigneev.tm.api;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.User;

import java.util.Collection;
import java.util.Scanner;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    User getCurrentUser();

    void setCurrentUser(final User currentUser);

    Scanner getScanner();

    Collection<AbstractCommand> getCommands();

}
