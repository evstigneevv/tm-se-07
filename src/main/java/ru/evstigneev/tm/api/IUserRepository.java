package ru.evstigneev.tm.api;

import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;

import java.util.Collection;

public interface IUserRepository {

    User create(final String login, final String password);

    User create(final String login, final String password, final RoleType role);

    boolean updatePassword(final String login, final String newPassword);

    User findByLogin(final String login);

    Collection<User> findAll();

    void update(final String userId, final String userName);

    boolean remove(final String userId);

    User persist(final String userId, final User user);

    User merge(final String userId, final User user);

    void removeAll();

}
