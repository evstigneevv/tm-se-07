package ru.evstigneev.tm.api;

import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.Collection;

public interface ITaskRepository {

    Task create(final String userId, final String taskName, final String projectId) throws EmptyStringException;

    boolean remove(final String userId, final String taskId) throws EmptyStringException;

    Task update(final String userId, final String taskId, final String taskName) throws EmptyStringException;

    Collection<Task> getTaskListByProjectId(final String userId, final String projectId) throws EmptyStringException;

    Collection<Task> findAll();

    Collection<Task> findAllByUserId(final String userId);

    boolean deleteAllProjectTasks(final String userId, final String projectId);

    Task findOne(final String userId, final String taskId) throws EmptyStringException;

    void removeAll();

    boolean removeAllByUserId(final String userId);

    Task merge(final String userId, final Task task) throws Exception;

    Task persist(final String userId, final Task task) throws Exception;

}
