package ru.evstigneev.tm.api;

import java.util.Collection;

public abstract class AbstractService<V> {

    public abstract V merge(final String userId, final V value) throws Exception;

    public abstract V persist(final String userId, final V value) throws Exception;

    public abstract void removeAll();

    public abstract Collection<V> findAll() throws Exception;

}
