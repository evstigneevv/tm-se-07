package ru.evstigneev.tm.enumerated;

public enum RoleType {

    ADMIN("ADMIN"),
    USER("USER");

    private String role;

    RoleType(final String role) {
        this.role = role;
    }

    public String displayName() {
        return role;
    }
}
