package ru.evstigneev.tm.repository;

import ru.evstigneev.tm.api.AbstractRepository;
import ru.evstigneev.tm.api.IUserRepository;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;

import java.util.Collection;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(final String login, final String password) {
        User user = new User(login, password);
        entities.put(user.getId(), user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final RoleType role) {
        User user = new User(login, password, role);
        entities.put(user.getId(), user);
        return user;
    }

    @Override
    public boolean updatePassword(final String login, final String newPassword) {
        Collection<User> userCollection = entities.values();
        for (User u : userCollection) {
            if (u.getLogin().equals(login)) {
                entities.get(u.getId()).setPasswordHash(newPassword);
                return true;
            }
        }
        return false;
    }

    @Override
    public User findByLogin(final String login) {
        Collection<User> userCollection = entities.values();
        for (User u : userCollection) {
            if (u.getLogin().equals(login)) {
                return u;
            }
        }
        return null;
    }

    @Override
    public Collection<User> findAll() {
        return entities.values();
    }

    @Override
    public void update(final String userId, final String userName) {
        entities.get(userId).setLogin(userName);
    }

    @Override
    public boolean remove(final String userId) {
        return entities.remove(userId) != null;
    }

    @Override
    public User merge(final String userId, final User user) {
        if (entities.get(userId).getRole().equals(RoleType.ADMIN)) {
            return entities.put(userId, user);
        }
        return null;
    }

    @Override
    public User persist(final String userId, final User user) {
        if (entities.get(userId).getRole().equals(RoleType.ADMIN)) {
            return entities.put(userId, user);
        }
        return null;
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

}