package ru.evstigneev.tm.repository;

import ru.evstigneev.tm.api.AbstractRepository;
import ru.evstigneev.tm.api.IProjectRepository;
import ru.evstigneev.tm.entity.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String userId, final String projectName) {
        Project project = new Project(userId, UUID.randomUUID().toString(), projectName);
        entities.put(project.getId(), project);
        return project;
    }

    @Override
    public boolean remove(final String userId, final String projectId) {
        if (entities.get(projectId).getUserId().equals(userId)) {
            return entities.remove(projectId) != null;
        } else return false;
    }

    @Override
    public Project update(final String userId, final String projectId, final String projectName) {
        if (entities.get(projectId).getUserId().equals(userId)) {
            entities.get(projectId).setName(projectName);
        }
        return entities.get(projectId);
    }

    @Override
    public Collection<Project> findAll() {
        return entities.values();
    }

    @Override
    public Collection<Project> findAllByUserId(final String userId) {
        Map<String, Project> userProjects = new LinkedHashMap<>();
        for (Map.Entry<String, Project> entry : entities.entrySet()) {
            if (entry.getValue().getUserId().equals(userId))
                userProjects.put(entry.getKey(), entry.getValue());
        }
        return userProjects.values();
    }

    @Override
    public Project findOne(final String userId, final String projectId) {
        if (entities.get(projectId).getUserId().equals(userId)) {
            return entities.get(projectId);
        }
        return null;
    }

    @Override
    public Project merge(final String userId, final Project project) {
        if (entities.get(project.getId()).getUserId().equals(userId)) {
            return entities.put(project.getId(), project);
        }
        return null;
    }

    @Override
    public Project persist(final String userId, final Project project) {
        if (entities.get(project.getId()).getUserId().equals(userId)) {
            return entities.put(project.getId(), project);
        }
        return null;
    }

    @Override
    public boolean removeAllByUserId(final String userId) {
        boolean isRemoved = false;
        for (Project p : findAllByUserId(userId)) {
            if (p.getUserId().equals(userId)) {
                entities.remove(p.getId());
                isRemoved = true;
            }
        }
        return isRemoved;
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

}
