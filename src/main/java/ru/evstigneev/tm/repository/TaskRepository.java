package ru.evstigneev.tm.repository;

import ru.evstigneev.tm.api.AbstractRepository;
import ru.evstigneev.tm.api.ITaskRepository;
import ru.evstigneev.tm.entity.Task;

import java.util.*;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String taskName, final String projectId) {
        Task task = new Task(userId, projectId, UUID.randomUUID().toString(), taskName);
        return entities.put(task.getId(), task);
    }

    @Override
    public boolean remove(final String userId, final String taskId) {
        if (entities.get(taskId).getUserId().equals(userId)) {
            return entities.remove(taskId) != null;
        }
        return false;
    }

    @Override
    public Task update(final String userId, final String taskId, final String taskName) {
        if (entities.get(taskId).getUserId().equals(userId)) {
            entities.get(taskId).setName(taskName);
        }
        return entities.get(taskId);
    }

    @Override
    public Collection<Task> getTaskListByProjectId(final String userId, final String projectId) {
        List<Task> taskListByProjectId = new ArrayList<>();
        Collection<Task> taskList = entities.values();
        for (Task t : taskList) {
            if (t.getProjectId().equals(projectId) && t.getUserId().equals(userId)) {
                taskListByProjectId.add(t);
            }
        }
        return taskListByProjectId;
    }

    @Override
    public Collection<Task> findAll() {
        return entities.values();
    }

    @Override
    public Collection<Task> findAllByUserId(final String userId) {
        Map<String, Task> userTasks = new LinkedHashMap<>();
        for (Map.Entry<String, Task> entry : entities.entrySet()) {
            if (entry.getValue().getUserId().equals(userId))
                userTasks.put(entry.getKey(), entry.getValue());
        }
        return userTasks.values();
    }

    @Override
    public boolean deleteAllProjectTasks(final String userId, final String projectId) {
        boolean isDeleted = false;
        Collection<Task> projectTasks = entities.values();
        for (Task t : projectTasks) {
            if (t.getProjectId().equals(projectId) && t.getUserId().equals(userId)) {
                entities.remove(t.getId());
                isDeleted = true;
            }
        }
        return isDeleted;
    }

    @Override
    public Task findOne(final String userId, final String taskId) {
        if (entities.get(taskId).getUserId().equals(userId)) {
            return entities.get(taskId);
        }
        return null;
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    public boolean removeAllByUserId(final String userId) {
        boolean isDeleted = false;
        Collection<Task> projectTasks = entities.values();
        for (Task t : projectTasks) {
            if (t.getUserId().equals(userId)) {
                entities.remove(t.getId());
                isDeleted = true;
            }
        }
        return isDeleted;
    }

    @Override
    public Task merge(final String userId, final Task task) {
        if (findOne(userId, task.getId()) != null) {
            entities.put(task.getId(), task);
        }
        return null;
    }

    @Override
    public Task persist(final String userId, final Task task) {
        if (entities.get(task.getId()).getUserId().equals(userId)) {
            return entities.put(task.getId(), task);
        }
        return null;
    }

}