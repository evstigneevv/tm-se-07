package ru.evstigneev.tm.command.system;

import ru.evstigneev.tm.command.AbstractCommand;

public class UserUpdatePasswordCommand extends AbstractCommand {

    @Override
    public String command() {
        return "UUP";
    }

    @Override
    public String description() {
        return "Update user password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("enter old password: ");
        String oldPassword = bootstrap.getScanner().nextLine();
        if (bootstrap.getUserService().checkPassword(bootstrap.getCurrentUser().getLogin(), oldPassword)) {
            System.out.println("enter new password: ");
            String newPassword = bootstrap.getScanner().nextLine();
            bootstrap.getUserService().updatePassword(bootstrap.getCurrentUser().getLogin(), newPassword);
            System.out.println("password was changed!");
        }
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
