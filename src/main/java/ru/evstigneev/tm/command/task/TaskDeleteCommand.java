package ru.evstigneev.tm.command.task;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.NoPermissionException;

public class TaskDeleteCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DT";
    }

    @Override
    public String description() {
        return "Delete task";
    }

    @Override
    public boolean havePermission(String first, String second) throws Exception {
        boolean isAdmin = bootstrap.getUserService().isAdmin(bootstrap.getCurrentUser());
        boolean isPermittedUser = !bootstrap.getCurrentUser().getLogin()
                .equals(bootstrap.getProjectService().findOne(first, second).getUserId());
        if (!isAdmin && !isPermittedUser) {
            throw new NoPermissionException();
        }
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Enter user ID: ");
        String userId = bootstrap.getScanner().nextLine();
        System.out.println("Enter task ID: ");
        String taskId = bootstrap.getScanner().nextLine();
        havePermission(userId, taskId);
        System.out.println("Enter task ID: ");
        if (bootstrap.getTaskService().remove(userId, taskId)) {
            System.out.println("Task was deleted!");
        } else {
            System.out.println("Task wasn't deleted!");
        }
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
