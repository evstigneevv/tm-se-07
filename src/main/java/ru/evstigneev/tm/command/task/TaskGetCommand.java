package ru.evstigneev.tm.command.task;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.RoleType;

import java.util.Collection;

import java.util.Collection;

public class TaskGetCommand extends AbstractCommand {

    @Override
    public String command() {
        return "ST";
    }

    @Override
    public String description() {
        return "Shows all tasks for specific project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("input user ID: ");
        String userId = bootstrap.getScanner().nextLine();
        System.out.println("input project ID: ");
        Collection<Task> taskList =
                bootstrap.getTaskService().getTaskListByProjectId(userId, bootstrap.getScanner().nextLine());
        System.out.println("|=====================================|=====================================|=====================================|");
        System.out.println("|          project ID                 |            task ID                  |               task name             |");
        System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
        for (Task t : taskList) {
            System.out.println("|" + t.getProjectId() + " |" + t.getId() + " | " + t.getName());
            System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
        }
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
