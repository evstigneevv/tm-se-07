package ru.evstigneev.tm.command.task;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.NoPermissionException;

public class TaskUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "TU";
    }

    @Override
    public String description() {
        return "Update task";
    }

    @Override
    public boolean havePermission(String first, String second) throws Exception {
        boolean isAdmin = bootstrap.getUserService().isAdmin(bootstrap.getCurrentUser());
        boolean isPermittedUser = !bootstrap.getCurrentUser().getLogin()
                .equals(bootstrap.getProjectService().findOne(first, second).getUserId());
        if (!isAdmin && !isPermittedUser) {
            throw new NoPermissionException();
        }
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("enter user ID");
        String userId = bootstrap.getScanner().nextLine();
        System.out.println("enter task ID");
        String taskId = bootstrap.getScanner().nextLine();
        havePermission(userId, taskId);
        bootstrap.getTaskService().update(userId, taskId, bootstrap.getScanner().nextLine());
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
