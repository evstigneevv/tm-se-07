package ru.evstigneev.tm.command.project;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.NoPermissionException;

public class ProjectDeleteCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DP";
    }

    @Override
    public String description() {
        return "Delete specify project";
    }

    @Override
    public boolean havePermission(String first, String second) throws Exception {
        boolean isAdmin = bootstrap.getUserService().isAdmin(bootstrap.getCurrentUser());
        boolean isPermittedUser = !bootstrap.getCurrentUser().getLogin()
                .equals(bootstrap.getProjectService().findOne(first, second).getUserId());
        if (!isAdmin && !isPermittedUser) {
            throw new NoPermissionException();
        }
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Input user ID: ");
        String userId = bootstrap.getScanner().nextLine();
        System.out.println("Enter project ID: ");
        String projectId = bootstrap.getScanner().nextLine();
        havePermission(userId, projectId);
        if (bootstrap.getProjectService().remove(userId, projectId)) {
            System.out.println("Project was deleted!");
        } else {
            System.out.println("Project wasn't deleted!");
        }
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
