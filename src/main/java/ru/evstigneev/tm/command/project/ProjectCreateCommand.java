package ru.evstigneev.tm.command.project;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;

public class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "CP";
    }

    @Override
    public String description() {
        return "Create a new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.print("input new project name: ");
        bootstrap.getProjectService().create(bootstrap.getCurrentUser().getId(), bootstrap.getScanner().nextLine());
        System.out.println("project created!");
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
