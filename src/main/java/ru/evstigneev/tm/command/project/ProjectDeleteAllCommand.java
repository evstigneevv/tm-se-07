package ru.evstigneev.tm.command.project;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;

public class ProjectDeleteAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DAP";
    }

    @Override
    public String description() {
        return "Delete all projects!";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getProjectService().removeAll();
        System.out.println("All projects deleted!");
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
