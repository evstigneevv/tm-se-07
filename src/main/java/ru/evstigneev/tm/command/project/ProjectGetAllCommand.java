package ru.evstigneev.tm.command.project;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.enumerated.RoleType;

public class ProjectGetAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SAP";
    }

    @Override
    public String description() {
        return "Show all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("|=====================================|=====================================|");
        System.out.println("|          project ID                 |            project name             |");
        System.out.println("|_____________________________________|_____________________________________|");
        for (Project p : bootstrap.getProjectService().findAll()) {
            System.out.println("|" + p.getId() + " | " + p.getName());
            System.out.println("|_____________________________________|_____________________________________|");
        }
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
