package ru.evstigneev.tm.service;

import ru.evstigneev.tm.api.AbstractService;
import ru.evstigneev.tm.api.IUserRepository;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.RepositoryException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;

public class UserService extends AbstractService<User> implements ru.evstigneev.tm.api.IUserService {

    private IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User create(final String login, final String password) throws Exception {
        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.create(login, password);
    }

    public User create(final String login, final String password, final RoleType role) throws Exception {
        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.create(login, password, role);
    }

    @Override
    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    public boolean checkPassword(final String login, final String password) throws Exception {
        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
            throw new EmptyStringException();
        }
        try {
            Collection<User> users = findAll();
            for (User u : users) {
                if (u.getLogin().equals(login)) {
                    return Arrays.toString(MessageDigest.getInstance("MD5").digest(password.getBytes())).equals(u.getPasswordHash());
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updatePassword(final String login, final String newPassword) throws Exception {
        if (login == null || newPassword == null || login.isEmpty() || newPassword.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.updatePassword(login, newPassword);
    }

    public boolean isAdmin(final User user) throws Exception {
        if (user == null) {
            throw new RepositoryException("there is no user!");
        }
        return user.getRole().equals(RoleType.ADMIN);
    }

    public User findByLogin(final String login) throws Exception {
        if (login == null || login.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.findByLogin(login);
    }

    public void update(final String userId, final String userName) throws Exception {
        if (userId == null || userName == null || userId.isEmpty() || userName.isEmpty()) {
            throw new EmptyStringException();
        }
        userRepository.update(userId, userName);
    }

    public boolean remove(final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.remove(userId);
    }

    @Override
    public User persist(final String userId, final User user) {
        return userRepository.persist(userId, user);
    }

    @Override
    public void removeAll() {
        userRepository.removeAll();
    }

    @Override
    public User merge(final String userId, final User user) {
        return userRepository.merge(userId, user);
    }

}
