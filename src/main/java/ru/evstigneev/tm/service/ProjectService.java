package ru.evstigneev.tm.service;

import ru.evstigneev.tm.api.AbstractService;
import ru.evstigneev.tm.api.IProjectRepository;
import ru.evstigneev.tm.api.ITaskRepository;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.RepositoryException;

import java.util.Collection;

public class ProjectService extends AbstractService<Project> implements ru.evstigneev.tm.api.IProjectService {

    private IProjectRepository projectRepository;
    private ITaskRepository taskRepository;

    public ProjectService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Collection<Project> findAll() throws Exception {
        if (projectRepository.findAll().isEmpty()) {
            throw new RepositoryException("No projects found!");
        }
        return projectRepository.findAll();
    }

    @Override
    public Project create(final String userId, final String projectName) throws EmptyStringException {
        if (userId == null || projectName == null || userId.isEmpty() || projectName.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.create(userId, projectName);
    }

    @Override
    public boolean remove(final String userId, final String projectId) throws EmptyStringException {
        if (userId == null || projectId == null || userId.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        taskRepository.deleteAllProjectTasks(userId, projectId);
        return projectRepository.remove(userId, projectId);
    }

    @Override
    public Project update(final String userId, final String projectId, final String newProjectName) throws EmptyStringException {
        if (userId == null || projectId == null || newProjectName == null || userId.isEmpty() || projectId.isEmpty() || newProjectName.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.update(userId, projectId, newProjectName);
    }

    @Override
    public Project findOne(final String userId, final String projectId) throws EmptyStringException {
        if (userId == null || projectId == null || userId.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.findOne(userId, projectId);
    }

    public Project merge(final String userId, final Project project) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (project == null) {
            throw new RepositoryException("No project!");
        }
        return projectRepository.merge(userId, project);
    }

    public Project persist(final String userId, final Project project) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (project == null) {
            throw new RepositoryException("No project!");
        }
        return projectRepository.persist(userId, project);
    }

    public Collection<Project> findAllByUserId(final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public void removeAll() {
        projectRepository.removeAll();
    }

    public boolean removeAllByUserId(String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        taskRepository.removeAllByUserId(userId);
        return projectRepository.removeAllByUserId(userId);
    }

}
