package ru.evstigneev.tm.service;

import ru.evstigneev.tm.api.AbstractService;
import ru.evstigneev.tm.api.ITaskRepository;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.RepositoryException;

import java.util.Collection;

public class TaskService extends AbstractService<Task> implements ru.evstigneev.tm.api.ITaskService {

    private ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String userId, final String projectId, final String taskName) throws EmptyStringException {
        if (userId == null || projectId == null || taskName == null || userId.isEmpty() || projectId.isEmpty() || taskName.isEmpty()) {
            return taskRepository.create(userId, projectId, taskName);
        }
        return null;
    }

    @Override
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    public Collection<Task> findAllByUserId(final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public boolean remove(final String userId, final String taskId) throws EmptyStringException {
        if (userId == null || taskId == null || userId.isEmpty() || taskId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.remove(userId, taskId);
    }

    @Override
    public Collection<Task> getTaskListByProjectId(final String userId, final String projectId) throws
            EmptyStringException {
        if (userId == null || projectId == null || userId.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.getTaskListByProjectId(userId, projectId);
    }

    @Override
    public Task update(final String userId, final String taskId, final String taskName) throws
            EmptyStringException {
        if (userId == null || taskId == null || taskName == null || userId.isEmpty() || taskId.isEmpty() || taskName.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.update(userId, taskId, taskName);
    }

    @Override
    public Task findOne(final String userId, final String taskId) throws EmptyStringException {
        if (userId == null || taskId == null || userId.isEmpty() || taskId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.findOne(userId, taskId);
    }

    public Task merge(final String userId, final Task task) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (task == null) {
            throw new RepositoryException("No project!");
        }
        return taskRepository.merge(userId, task);
    }

    public Task persist(final String userId, final Task task) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (task == null) {
            throw new RepositoryException("No project!");
        }
        return taskRepository.persist(userId, task);
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public boolean deleteAllProjectTasks(final String userId, final String projectId) throws Exception {
        if (userId == null || projectId == null || userId.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.deleteAllProjectTasks(userId, projectId);
    }

    public boolean removeAllByUserId(final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.removeAllByUserId(userId);
    }

}
