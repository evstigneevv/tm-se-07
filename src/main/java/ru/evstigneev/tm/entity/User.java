package ru.evstigneev.tm.entity;

import ru.evstigneev.tm.enumerated.RoleType;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.UUID;

public class User {

    private String login;
    private String id;
    private String passwordHash;
    private RoleType role;

    public User(final String login, final String password) {
        this.login = login;
        id = UUID.randomUUID().toString();
        setPasswordHash(password);
        role = RoleType.ADMIN;
    }

    public User(final String login, final String password, final RoleType isAdmin) {
        this.login = login;
        id = UUID.randomUUID().toString();
        setPasswordHash(password);
        this.role = isAdmin;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(final String password) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            passwordHash = Arrays.toString(messageDigest.digest(password.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public RoleType getRole() {
        return role;
    }

    public void setRole(final RoleType role) {
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

}
